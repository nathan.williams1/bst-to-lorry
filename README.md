bst to lorry
===

## What is this?

A script that takes a BuildStream project, reads through the dependency
elements and outputs a set of `.lorry` files for use in setting up lorry
mirroring.

## How to use it?

Remember to be in a project directory which is using BuildStream.

Running the `bst-to-lorry.py` script will generate the Lorry files for a given BuildStream element.
You must pass the BuildStream elements as arguments when running this script,
and there is additional optional arguments to define where the files are output.
```bash
python3 path/to/bst-to-lorry.py element1.bst element2.bst --git-directory git-mirrors/ --raw-files-directory raw-file-mirrors/
```

The above would create two directories within the working directory called `git-mirrors` and `raw-files-mirrors`.  Inside the former would be all the git mirror lorry files, and in the latter the raw file lorry files.
